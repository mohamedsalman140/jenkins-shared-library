#!/usr/bin/env groovy

package com.example.Docker

def call(String imageName) {
    return new Docker(this).Dockerbuildimage(imageName)
}