#!/usr/bin/env groovy
def call(){
    def branchName = env.BRANCH_NAME   ? 'master' : 'pipelinejobs'

    // Display the branch name
    echo "Building JAR for branch: ${branchName}"

    sh "mvn package"

}