#!/usr/bin/env groovy
package com.example
class Docker implements Serializable{
    def script

    Docker(script){
        this.script=script
    }
    def Dockerbuildimage(String imageName){

        script.sh("curl -u $DOCKERHUBREPO_USR:$DOCKERHUBREPO_PSW https://hub.docker.com/")
        // Build Docker image
        script.sh "docker build -t $imageName ."
        // Push Docker image to Dockerhub
        script.sh "docker push $imageName"

    }
}